package coreExecrises;

public class PalindromicArrays {

	public static void main(String[] args) {

		String[] breakfast = { "Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms" };
		String[] palindromic = { "Sausage", "Eggs", "Beans", "Beans", "Eggs", "Sausage" };

		System.out.println("*** Output ***");
		System.out.println(isPalindromic(palindromic));
		System.out.println(isPalindromic(breakfast));
	}

	public static boolean isPalindromic(String[] arr) {
		boolean isPal = true;
		for (int i = 0; i < arr.length; i++) {
			if (!arr[i].equals(arr[arr.length - 1 - i])) {
				isPal = false;
			}
		}
		return isPal;
	}

}
