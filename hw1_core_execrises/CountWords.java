package coreExecrises;

import java.util.ArrayList;

public class CountWords {

	public static void main(String[] args) {

		String s = "I never saw a purple cow";

		System.out.println("*** Output ***");
		System.out.println(countWords(s));
	}

	public static int countWords(String s) {
		int count = 0;
		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < s.length(); i++) {
			arr.add((String) s.subSequence(i, i + 1));
		}
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).equals(" ")) {
				count++;
			}
		}

		return count + 1;
	}
}
