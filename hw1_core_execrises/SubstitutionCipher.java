package coreExecrises;

import java.util.ArrayList;

public class SubstitutionCipher {

	public static void main(String[] args) {

		String s = "Hello World";

		System.out.println("*** Output ***");
		encode(s);
	}

	public static void encode(String str) {
		ArrayList<String> arr = new ArrayList<String>();
		ArrayList<String> s = new ArrayList<String>();

		arr.add("a");
		arr.add("b");
		arr.add("c");
		arr.add("d");
		arr.add("e");
		arr.add("f");
		arr.add("g");
		arr.add("h");
		arr.add("i");
		arr.add("j");
		arr.add("k");
		arr.add("l");
		arr.add("m");
		arr.add("n");
		arr.add("o");
		arr.add("p");
		arr.add("q");
		arr.add("r");
		arr.add("s");
		arr.add("t");
		arr.add("u");
		arr.add("v");
		arr.add("w");
		arr.add("x");
		arr.add("y");
		arr.add("z");

		for (int i = 0; i < str.length(); i++) {
			s.add((String) str.subSequence(i, i + 1));
		}

		for (int i = 0; i < s.size(); i++) {
			for (int j = 0; j < arr.size(); j++) {
				if (s.get(i).toLowerCase().equals(arr.get(j))) {
					if (i < s.size() - 1) {
						System.out.print(j + 1 + ",");
					} else {
						System.out.print(j + 1);
					}
				}
			}
		}
	}

}
