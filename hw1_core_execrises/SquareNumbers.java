package coreExecrises;

import java.util.ArrayList;

public class SquareNumbers {

	public static void main(String[] args) {

		int lessThan = 100;
		ArrayList<Integer> arr = new ArrayList<Integer>();
		double sqrt = Math.sqrt(lessThan);

		for (int i = 1; i <= sqrt; i++) {
			arr.add(i * i);
		}

//		System.out.print("Output:"+arr);

		for (int i = 1; i <= arr.size() + 2; i++) {
			System.out.print(i + ": ");
			if (i == 1) {
				System.out.println("squares()");
			} else if (i == 2) {
				System.out.println("*** Output ***");
			} else {
				System.out.println(arr.get(i - 3));
			}
		}

	}

}
