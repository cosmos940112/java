package coreExecrises;

import java.util.ArrayList;

public class CountAlphanumerics {

	public static void main(String[] args) {

		String s = "1984 by George Orwell.";

		System.out.println("*** Output ***");
		System.out.println(countEs(s));
	}

	public static int countEs(String s) {
		int count = 0;
		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < s.length(); i++) {
			arr.add((String) s.subSequence(i, i + 1));
		}
		for (int i = 0; i < arr.size(); i++) {
			if (!arr.get(i).equals(" ") && !arr.get(i).equals(".")) {
				count++;
			}
		}

		return count;
	}

}
