package coreExecrises;

import java.util.ArrayList;

public class RemoveVowels {

	public static void main(String[] args) {

		String s = "I never saw a purple cow";

		System.out.println("*** Output ***");
		star(s);
	}

	public static void star(String str) {
		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < str.length(); i++) {
			arr.add((String) str.subSequence(i, i + 1));
		}

		for (int i = 0; i < arr.size(); i++) {
//			System.out.print(i + "��:" + arr.get(i) + ":" + arr.get(arr.size() - i - 1) + "--------");
			if (arr.get(i).toLowerCase().equals("a") || arr.get(i).toLowerCase().equals("e")
					|| arr.get(i).toLowerCase().equals("i") || arr.get(i).toLowerCase().equals("o")
					|| arr.get(i).toLowerCase().equals("u")) {
				arr.set(i, "*");
			}
		}

		for (int i = 0; i < arr.size(); i++) {
			System.out.print(arr.get(i));
		}
	}

}
