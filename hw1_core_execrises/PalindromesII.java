package coreExecrises;

import java.util.ArrayList;

public class PalindromesII {
	
	public static void main(String[] args) {

		String s = "I never saw a purple cow";
		String p = "Rise to vote, Sir!";

		System.out.println("*** Output ***");
		System.out.println(isPalindrome(s));
		System.out.println(isPalindrome(p));
	}

	public static boolean isPalindrome(String str) {
		boolean isPal = true;
		ArrayList<String> arr = new ArrayList<String>();

		String[] inpArr = str.split(" |,|!|:");

		for (int i = 0; i < inpArr.length; i++) {
			for (int j = 0; j < inpArr[i].length(); j++) {
				arr.add((String) inpArr[i].subSequence(j, j + 1));
			}
		}

		for (int i = 0; i < arr.size() / 2 + 1; i++) {
//			System.out.print(i + "��:" + arr.get(i) + ":" + arr.get(arr.size() - i - 1) + "--------");
			if (arr.get(i).toLowerCase().equals(arr.get(arr.size() - i - 1).toLowerCase())) {
				isPal = true;
			} else {
				isPal = false;
				break;
			}
//			System.out.println(isPal);
		}
		return isPal;
	}

}
