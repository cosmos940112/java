package coreExecrises;

import java.util.ArrayList;

public class CountTheLetterE {

	public static void main(String[] args) {

		String s = "I never saw a purple cow";

		System.out.println("*** Output ***");
		System.out.println(countEs(s));
	}

	public static int countEs(String s) {
		int count = 0;
		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < s.length(); i++) {
			arr.add((String) s.subSequence(i, i + 1));
		}
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).equals("e")) {
				count++;
			}
		}

		return count;
	}

}
