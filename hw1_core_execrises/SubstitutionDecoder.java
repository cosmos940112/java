package coreExecrises;

import java.util.ArrayList;

public class SubstitutionDecoder {

	public static void main(String[] args) {

		String s = "9 14,5,22,5,18 19,1,23 1 16,21,18,16,12,5 3,15,23";

		System.out.println("*** Output ***");
		decode(s);
	}

	public static void decode(String str) {
		ArrayList<String> arr = new ArrayList<String>();
		ArrayList<String> s = new ArrayList<String>();

		arr.add("a");
		arr.add("b");
		arr.add("c");
		arr.add("d");
		arr.add("e");
		arr.add("f");
		arr.add("g");
		arr.add("h");
		arr.add("i");
		arr.add("j");
		arr.add("k");
		arr.add("l");
		arr.add("m");
		arr.add("n");
		arr.add("o");
		arr.add("p");
		arr.add("q");
		arr.add("r");
		arr.add("s");
		arr.add("t");
		arr.add("u");
		arr.add("v");
		arr.add("w");
		arr.add("x");
		arr.add("y");
		arr.add("z");

		String[] split = str.split(",");

		for (int i = 0; i < split.length; i++) {
			s.add((String) split[i]);
		}

		System.out.println(s);

		for (int i = 0; i < s.size(); i++) {
				if (s.get(i).contains(" ")) {
					String[] temp = s.get(i).split(" ");
					for(int j=0;j<temp.length;j++) {
						System.out.print(arr.get(Integer.valueOf(temp[j])-1));
						if(j<temp.length-1) {
							System.out.print(" ");
						}
					}
				} else {
					System.out.print(arr.get(Integer.valueOf(s.get(i)) - 1));
				}
			
		}
	}

}
