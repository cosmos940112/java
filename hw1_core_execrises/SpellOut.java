package coreExecrises;

import java.util.ArrayList;

public class SpellOut {

	public static void main(String[] args) {

		String s = "I never saw a purple cow";

		System.out.println("*** Output ***");
		spellOut(s);
	}

	public static void spellOut(String str) {
		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < str.length(); i++) {
			arr.add((String) str.subSequence(i, i + 1));
		}

		for (int i = 0; i < arr.size(); i++) {
			if (i < arr.size() - 1) {
				if (!arr.get(i + 1).equals(" ") && !arr.get(i).equals(" ")) {
					arr.set(i, arr.get(i) + "-");
				}
			}
		}

		for (int i = 0; i < arr.size(); i++) {
			System.out.print(arr.get(i).toUpperCase());
		}
	}

}
