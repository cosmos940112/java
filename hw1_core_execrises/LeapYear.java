package coreExecrises;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {

		System.out.print("Input the year:");

		Scanner sc = new Scanner(System.in);

		String inpS = sc.nextLine();

		int inp = Integer.parseInt(inpS);

		if (inp % 4 == 0) {
			System.out.print(inp + " is a leap year");
		} else {
			System.out.print(inp + " isn't a leap year");
		}

	}

}
