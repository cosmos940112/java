package coreExecrises;

public class ReverseAnArray {

	public static void main(String[] args) {

		String[] breakfast = { "Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms" };

		System.out.println("*** Output ***");
		reverse(breakfast);

	}

	public static String[] reverse(String[] arr) {
		String[] tempArr = new String[arr.length];
		for (int i = 0; i < arr.length; i++) {
			tempArr[i] = arr[arr.length - 1 - i];
		}
		arr = tempArr;
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		return arr;
	}

}
