package coreExecrises;

import java.util.ArrayList;

public class PackDuplicates {

	public static void main(String[] args) {

		String[] letters = { "a", "a", "a", "a", "b", "c", "c", "a", "a", "d", "e", "e", "e", "e" };

		System.out.println("*** Output ***");
		pack(letters);
	}

	public static void pack(String[] nums) {
		ArrayList<String> arr = new ArrayList<String>();
		int count = 0;

		for (int i = 0; i < nums.length - 1; i++) {
			if (i == 0) {
				arr.add(nums[i]);
			}
			if (nums[i] == nums[i + 1]) {
				arr.set(count, arr.get(count) + nums[i + 1]);
			} else {
				arr.add(nums[i + 1]);
				count++;
			}
		}
		System.out.println(arr);
	}

}
