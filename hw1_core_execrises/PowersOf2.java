package coreExecrises;

import java.util.ArrayList;

public class PowersOf2 {

	public static void main(String[] args) {

		int num = 2;
		int power = 8;
		ArrayList<Integer> arr = new ArrayList<Integer>();

		for (int i = 1; i <= power; i++) {
			arr.add((int) Math.pow(num, i));
		}

//		System.out.print("Output:"+arr);

		for (int i = 1; i <= arr.size() + 2; i++) {
			System.out.print(i + ": ");
			if (i == 1) {
				System.out.println("powers(" + power + ")");
			} else if (i == 2) {
				System.out.println("*** Output ***");
			} else {
				System.out.println(arr.get(i - 3));
			}
		}

	}

}
