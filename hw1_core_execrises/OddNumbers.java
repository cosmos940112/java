package coreExecrises;

import java.util.ArrayList;

public class OddNumbers {
	
	public static void main(String[] args) {

		int lessThan=20;
		ArrayList<Integer> arr = new ArrayList<Integer>();
		
		for (int i = 0; i < lessThan; i++) {
			if(i%2!=0) {
				arr.add(i);
			}
		}
		
//		System.out.print("Output:"+arr);

		for (int i = 1; i <= arr.size()+2; i++) {
			System.out.print(i+": ");
			if(i==1) {
				System.out.println("oddNumbers()");
			}else if(i==2){
				System.out.println("*** Output ***");
			}else {
				System.out.println(arr.get(i-3));
			}
		}
		
	}

}
