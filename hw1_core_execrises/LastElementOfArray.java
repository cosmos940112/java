package coreExecrises;

public class LastElementOfArray {

	public static void main(String[] args) {

		String[] breakfast = { "Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms" };

		System.out.println("*** Output ***");
		System.out.println(lastElement(breakfast));
	}

	public static String lastElement(String[] arr) {
		return arr[arr.length - 1];
	}

}
