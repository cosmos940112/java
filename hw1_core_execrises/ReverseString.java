package coreExecrises;

import java.util.ArrayList;

public class ReverseString {

	public static void main(String[] args) {

		String s = "I never saw a purple cow";

		System.out.println("*** Output ***");
		reverse(s);
	}

	public static void reverse(String s) {
		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < s.length(); i++) {
			arr.add((String) s.subSequence(i, i + 1));
		}
		for (int i = 0; i < arr.size(); i++) {
			System.out.print(arr.get(arr.size() - 1 - i));
		}
	}

}
