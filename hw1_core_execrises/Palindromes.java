package coreExecrises;

import java.util.ArrayList;

public class Palindromes {

	public static void main(String[] args) {

		String sentence = "I never saw a purple cow";
		String palindrome = "rotavator";

		System.out.println("*** Output ***");
		System.out.println(isPalindrome(sentence));
		System.out.println(isPalindrome(palindrome));
	}

	public static boolean isPalindrome(String str) {
		boolean isPal = true;
		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < str.length(); i++) {
			arr.add((String) str.subSequence(i, i + 1));
		}
		for (int i = 0; i < arr.size(); i++) {
			if (!arr.get(i).equals(arr.get(arr.size() - i - 1))) {
				isPal = false;
			}
		}
		return isPal;
	}

}
