package advanceExercises;

import java.util.ArrayList;
import java.util.Scanner;

public class validPalindrome {

	public static void main(String[] args) {

		System.out.print("Input:");// "A man, a plan, a canal: Panama"

		Scanner sc = new Scanner(System.in);

		String inpS = sc.nextLine();

		String[] inpArr = inpS.split(" |\"|,|:");

		ArrayList<String> arr = new ArrayList<String>();

		for (int i = 0; i < inpArr.length; i++) {
			for (int j = 0; j < inpArr[i].length(); j++) {
				arr.add((String) inpArr[i].subSequence(j, j + 1));
			}
		}

		for (int i = 0; i < arr.size(); i++) {
			System.out.print(arr.get(i).toLowerCase());
		}

		System.out.println("");

		boolean isPal = false;
		for (int i = 0; i < arr.size() / 2 + 1; i++) {
//			System.out.print(i + "��:" + arr.get(i) + ":" + arr.get(arr.size() - i - 1) + "--------");
			if (arr.get(i).toLowerCase().equals(arr.get(arr.size() - i - 1).toLowerCase())) {
				isPal = true;
			} else {
				isPal = false;
				break;
			}
//			System.out.println(isPal);
		}
		System.out.println("Output:" + isPal);
	}

}
