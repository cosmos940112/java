package advanceExercises;

import java.util.ArrayList;
import java.util.Scanner;

public class perfectNumber {

	public static void main(String[] args) {

		System.out.print("Input:");

		Scanner sc = new Scanner(System.in);

		String inpS = sc.nextLine();

		int inp = Integer.parseInt(inpS);

		int sum = 0;
		ArrayList<Integer> arr = new ArrayList<Integer>();

		if (inp > 100000000) {
			System.out.println("The input number n will not exceed 100,000,000. (1e8)");
		} else {

			for (int i = 1; i < inp; i++) {
				if (inp % i == 0) {
					arr.add(i);
				}
			}

			for (int i = 0; i < arr.size(); i++) {
				sum = sum + arr.get(i);
			}

			if (sum == inp) {
				System.out.println("Output:True");
				System.out.print("Explanation:" + inp + "=");
				for (int i = 0; i < arr.size(); i++) {
					if (i == arr.size() - 1) {
						System.out.print(arr.get(i));
					} else {
						System.out.print(arr.get(i) + "+");
					}
				}
			} else {
				System.out.print("Output:False");
			}

		}
	}

}
