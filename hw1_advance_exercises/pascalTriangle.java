package advanceExercises;

import java.util.ArrayList;
import java.util.Scanner;

public class pascalTriangle {

	public static void main(String[] args) {

		System.out.print("Input:");

		Scanner sc = new Scanner(System.in);

		String inpS = sc.nextLine();

		int inp = Integer.parseInt(inpS);

		int[][] triangle = new int[inp][inp];
		
		System.out.println("Output:");
		System.out.println("[");

		for (int i = 0; i < triangle.length; i++) {
			triangle[i][0] = 1;
			for (int s = 0; s < triangle.length - i; s++) {
				System.out.print(" ");
			}
			System.out.print("[");
			for (int j = 0; j < i + 1; j++) {
				if (j != 0) {
					triangle[i][j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
				}

				if (j == 0) {
					System.out.print(triangle[i][j]);

				} else {
					System.out.print("," + triangle[i][j]);
				}
			}
			if (i == triangle.length-1) {
				System.out.println("]");

			} else {
				System.out.println("],");
			}
		}
		
		System.out.println("]");

	}

}
