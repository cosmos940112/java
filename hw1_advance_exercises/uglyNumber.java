package advanceExercises;

import java.util.ArrayList;
import java.util.Scanner;

public class uglyNumber {

	public static void main(String[] args) {

		System.out.print("Input:");

		Scanner sc = new Scanner(System.in);

		String inpS = sc.nextLine();

		int inp = Integer.parseInt(inpS);

		boolean uglyCheck = true;
		int inpTemp = 0;
		ArrayList<Integer> arr = new ArrayList<Integer>();

		if (inp > 100000000) {
			System.out.println("The input number n will not exceed 100,000,000. (1e8)");
		} else {
			
			inpTemp=inp;

			while (inp % 2 == 0) {
				inp = inp / 2;
				arr.add(2);
			}

			while (inp % 3 == 0) {
				inp = inp / 3;
				arr.add(3);
			}

			while (inp % 5 == 0) {
				inp = inp / 5;
				arr.add(5);
			}

			if (inp == 1) {
				for (int i = 0; i < arr.size(); i++) {
					if (arr.get(i) != 2 && arr.get(i) != 3 && arr.get(i) != 5) {
						uglyCheck = false;
					}
				}

				if (uglyCheck) {
					System.out.println("Output:True");
					System.out.print("Explanation:" + inpTemp + "=");
					for (int i = 0; i < arr.size(); i++) {
						if (i == arr.size() - 1) {
							System.out.print(arr.get(i));
						} else {
							System.out.print(arr.get(i) + "��");
						}
					}
				}
			} else {
				System.out.println("Output:False");
				System.out.print("Explanation:" + inpTemp + " is not ugly since it includes another prime factor "+inp+".");
			}

		}
	}

}
