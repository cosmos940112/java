package advanceExercises;

import java.util.ArrayList;
import java.util.Scanner;

public class palindromeNumber {

	public static void main(String[] args) {

		ArrayList<Integer> arr = new ArrayList<Integer>();

//      System.out.println("23%10:" + 23 % 10);

		System.out.print("Input:");

		Scanner sc = new Scanner(System.in);

		String inpS = sc.nextLine();

		String reverseInp = "";

		int inp = Integer.parseInt(inpS);

		int len = String.valueOf(inp).length();

		for (int i = 0; i < len; i++) {
//			System.out.println("i:" + i);
			if (i == 0) {
				arr.add(inp % index(10, i + 1));
			} else {
				arr.add((inp % index(10, i + 1) - arr.get(i - 1)) / index(10, i));
			}
		}

		for (int i = 0; i < len; i++) {
			reverseInp = reverseInp + Integer.toString(arr.get(i));
		}

		if (inpS.equals(reverseInp)) {
			System.out.println("Output:true");
//			System.out.print("Explanation:" + inpS + "=" + reverseInp);
		} else {
			System.out.println("Output:false");
			System.out.print("Explanation:From left to right, it reads " + inpS + ". From right to left, it becomes "
					+ reverseInp + ". Therefore it is not a palindrome.");
		}

	}

	// �����
	public static int index(int num, int power) {
		int returnNum = 1;
		for (int i = 0; i < power; i++) {
			returnNum = returnNum * num;
		}
		return returnNum;
	}

}
