package advanceExercises;

import java.util.ArrayList;
import java.util.Scanner;

public class validParentheses {

	public static void main(String[] args) {

		System.out.print("Input:");

		Scanner sc = new Scanner(System.in);

		String inpS = sc.nextLine();

		boolean validCheck = true;
		ArrayList<String> arr = new ArrayList<String>();
		ArrayList<Integer> tempArr = new ArrayList<Integer>();

		if (inpS.contains("(") || inpS.contains("[") || inpS.contains("{")) {

			for (int i = 0; i < inpS.length(); i++) {
				arr.add((String) inpS.subSequence(i, i + 1));
			}
//			System.out.println(arr);
			for (int i = 0; i < arr.size(); i++) {
				if (arr.get(i).equals(")")) {
					arr.set(i, "(");
				}
				if (arr.get(i).equals("]")) {
					arr.set(i, "[");
				}
				if (arr.get(i).equals("}")) {
					arr.set(i, "{");
				}
			}
//			System.out.println(arr);// ({[]{}}) ()[]{}

			for (int i = 0; i < arr.size() / 2; i++) {
				if (arr.get(i).equals(arr.get(arr.size() - 1 - i))) {
					tempArr.add(i);
					tempArr.add(arr.size() - 1 - i);
				}
			}

			if (validCheck) {
				tempArr.sort(null);

				for (int i = tempArr.size() - 1; i >= 0; i--) {
					int a = tempArr.get(i);
					arr.remove(a);
				}
//				System.out.println(arr);
				for (int i = 0; i < arr.size() / 2; i++) {
					if (arr.get(arr.size() - 1 - i).equals(arr.get(arr.size() - 2 - i))) {
						arr.remove(arr.size() - 1);
						arr.remove(arr.size() - 1);
					} else {
						validCheck = false;
						break;
					}
				}
//				System.out.println(arr);
				System.out.println("Output:" + validCheck);
			} else {
				System.out.println("Output:" + validCheck);
			}
		} else {
			System.out.println("plz contains \"(\",\"[\",\"{\" ");
		}

	}

}
