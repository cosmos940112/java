package advanceExercises;

import java.util.ArrayList;
import java.util.Scanner;

public class twoSum {

	public static void main(String[] args) {

		ArrayList<Integer> givenNum = new ArrayList<Integer>();
		givenNum.add(2);
		givenNum.add(7);
		givenNum.add(11);
		givenNum.add(15);

		System.out.print("Given nums =" + givenNum);
		System.out.print(",target = ");

		Scanner sc = new Scanner(System.in);

		String targetS = sc.nextLine();

		int target = Integer.parseInt(targetS);

		for (int i = 0; i < givenNum.size(); i++) {
			for (int j = i + 1; j < givenNum.size(); j++) {
				if (givenNum.get(i) + givenNum.get(j) == target) {
					System.out.println("Because nums[" + i + "] + nums[" + j + "] = " + givenNum.get(i) + " + "
							+ givenNum.get(j) + " = " + (givenNum.get(i) + givenNum.get(j)) + ",");
					System.out.print("return[" + i + "," + j + "].");
				}
			}
		}
		System.out.println();

	}

}
